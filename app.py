from flask import Flask, jsonify
import json
import pandas as pd
from functions import get_all_transcriptions
from functions import calculate_word_frequencies,check_keywords,extract_customer_transcripts,extract_agent_transcripts,get_audio_duration,calculate_cross_talks,process_annotations,read_rttm_file,analyze_silences

app = Flask(__name__)

@app.route('/')
def fetch_data():
    json_path = "uploads/data.json"
    rttm_path="uploads/rttm_data.rttm"

    #Code to get all transcript
    try:
        #Get the total_transcript
        all_transcriptions = get_all_transcriptions(json_path)
        #Get the agent_transcript
        agent_transcript=extract_agent_transcripts(json_path)
        #Customer_Transcript
        customer_transcript=extract_customer_transcripts(json_path)

        #Create a df of annotations
        annotations = read_rttm_file(rttm_path)
        df = process_annotations(annotations)
        # Filter out 'Silence' entries
        df_no_silent = df[df['Speaker_id'] != 'Silence']
        customer_talk_duration, agent_talk_duration, cross_talk_duration, cross_talks, longest_cross_talk_duration, total_call_duration = calculate_cross_talks(df_no_silent)
        # Step 1: Filter the silences from the DataFrame
        df_filtered_silence = df[df['Speaker_id'] == 'Silence']
        # Step 2: Calculate the duration of each silence
        df_filtered_silence['Duration'] = df_filtered_silence['End_time'] - df_filtered_silence['Start_time']
        # Step 3: Filter out silences that are more than 5 seconds long
        df_long_silences = df_filtered_silence[df_filtered_silence['Duration'] > 5]

        # Convert DataFrame to dictionary
        df_long_silences_dict = df_long_silences.to_dict(orient='records')

        total_silences, total_duration, longest_duration = analyze_silences(df)

        return jsonify({"transcriptions": all_transcriptions,
                        "agent_transcript":agent_transcript,
                        "customer_transcript":customer_transcript,
                        "customer_talk_duration": customer_talk_duration,
                        "agent_talk_duration": agent_talk_duration,
                        "cross_talk_duration": cross_talk_duration,
                        "longest_cross_talk_duration": longest_cross_talk_duration,
                        "total_call_duration": total_call_duration,
                        "cross-talk_instances":cross_talks,
                        "silence_instances":total_silences,
                        "total_silence_duration":total_duration,
                        "longest_silence_duration":longest_duration,
                        "df_long_silences_dict":df_long_silences_dict,
                        "hold_instances":total_silences
                        }), 200
    except Exception as e:
        return jsonify({"error": f"Failed to retrieve transcriptions: {str(e)}"}), 500

if __name__ == '__main__':
    app.run(debug=True)