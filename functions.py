import json
import os
import requests
from flask import Flask, render_template, request, jsonify, send_file
#import single_file_inference as infer
from pydub import AudioSegment
import pandas as pd
import uuid
import time

#from single_file_inference import Wav2VecCtc
# from speechbrain.pretrained.interfaces import foreign_class
from flask import request, jsonify
from pydub import AudioSegment
import os


#Function to get all the transcripts
def get_all_transcriptions(json_file_path):
    all_transcriptions = ""

    try:
        with open(json_file_path, 'r') as file:
            data = json.load(file)

            for entry in data:
                if 'transcription' in entry:
                    all_transcriptions += entry['transcription']

        return all_transcriptions
    except Exception as e:
        print(f"An error occurred: {str(e)}")
        return None


# Function to find word frequency
def calculate_word_frequencies(text):
    # Get all stop words from NLTK corpus
    # stop_words = set(stopwords.words('english'))
    stop_words = [
        'i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd",
        'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers',
        'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which',
        'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been',
        'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if',
        'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between',
        'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out',
        'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why',
        'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not',
        'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't",
        'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't", 'couldn',
        "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't",
        'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', "needn't", 'shan', "shan't",
        'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn', "wouldn't", "give",
        "take"
    ]

    # Split the text into words
    words = text.split()

    # Initialize a dictionary to store word frequencies
    word_freq = {}

    # Iterate through each word in the text
    for word in words:
        # Convert word to lowercase for case-insensitivity
        word = word.lower()

        # Check if the word is not a stop word
        if word not in stop_words:
            # Update word frequency count
            if word in word_freq:
                word_freq[word] += 1
            else:
                word_freq[word] = 1

    # Sort the word frequencies in descending order
    sorted_word_freq = sorted(word_freq.items(), key=lambda x: x[1], reverse=True)

    # Consider only the top 5 words
    top_word_freq = sorted_word_freq[:5]

    # Convert the top 5 word frequency dictionary to a list of objects
    word_freq_objects = [{'word': word, 'frequency': freq} for word, freq in top_word_freq]

    return word_freq_objects


# Function to get agent_transcript
def extract_agent_transcripts(json_file_path):
    agent_transcripts = ''

    with open(json_file_path, 'r', encoding='utf-8') as json_file:
        data = json.load(json_file)

        for row in data:
            speaker_id = row.get('speaker_id', '').lower()
            actual_transcript = row.get('actual_transcript', '').strip().lower()

            if speaker_id == 'agent':
                agent_transcripts += actual_transcript + " "

    return agent_transcripts


# Function to get customer_transcript
def extract_customer_transcripts(json_file_path):
    customer_transcripts = ''

    with open(json_file_path, 'r', encoding='utf-8') as json_file:
        data = json.load(json_file)

        for row in data:
            speaker_id = row.get('speaker_id', '').lower()
            actual_transcript = row.get('actual_transcript', '').strip().lower()

            if speaker_id == 'customer':
                customer_transcripts += actual_transcript + " "

    return customer_transcripts


# Function to find Tagging Parameters

def check_keywords(transcript, tags):
    tagging_parameters = {}
    for tag in tags:
        if tag in transcript:
            tagging_parameters[tag] = 'yes'
        else:
            tagging_parameters[tag] = 'no'
    return tagging_parameters


# Define a function to calculate the duration of the audio file
def get_audio_duration(file_path):
    audio = AudioSegment.from_file(file_path)
    duration_ms = len(audio)
    duration_seconds = duration_ms / 1000

    # Calculate minutes and seconds
    minutes = int(duration_seconds // 60)
    seconds = int(duration_seconds % 60)

    return f"{minutes} minutes {seconds} seconds"


# Read the rffm file
def read_rttm_file(file_path):
    annotations = []

    with open(file_path, 'r') as rttm_file:
        for line in rttm_file:
            fields = line.strip().split()

            annotation_type = fields[0]
            audio_id = fields[1]
            channel = fields[2]
            start_time = float(fields[3])
            duration = float(fields[4])
            label = fields[7]

            annotations.append({
                'annotation_type': annotation_type,
                'audio_id': audio_id,
                'channel': channel,
                'start_time': start_time,
                'end_time': start_time + duration,
                'label': label
            })

    return annotations


# classifier = foreign_class(source="speechbrain/emotion-recognition-wav2vec2-IEMOCAP",
#               pymodule_file="custom_interface.py", classname="CustomEncoderWav2vec2Classifier")
# Define a function to handle exceptions and assign the "error" label
# def classify_with_error_handling(audio_segment_path):
#   try:
#      out_prob, score, _, text_lab = classifier.classify_file(audio_segment_path)
#    return text_lab
# except Exception as e:
#    print(f"Error processing audio: {e}")
#   return "error"

# Function to annotate the rttm data
def process_annotations(annotations):
    end_time = 0
    label_mapping = {'SPEAKER_00': 'Agent', 'SPEAKER_01': 'Customer'}
    data = []

    try:
        for annotation in annotations:
            if end_time != annotation['start_time'] and end_time < annotation['start_time']:
                start_time = annotation['start_time']
                data.append({'Start_time': end_time, 'End_time': start_time, 'Speaker_id': 'Silence'})

            start_time = annotation['start_time']
            end_time = annotation['end_time']
            label = label_mapping.get(annotation['label'], annotation['label'])
            data.append({'Start_time': start_time, 'End_time': end_time, 'Speaker_id': label})
    except Exception as e:
        # Print or log the exception
        print(f"Error: {e}")

    # Convert the list of dictionaries to a DataFrame
    df = pd.DataFrame(data)
    return df


# Calculate Cross Talks
def calculate_cross_talks(df):
    customer_talk_duration = 0
    agent_talk_duration = 0
    cross_talk_duration = 0
    longest_cross_talk_duration = 0
    cross_talks = 0

    previous_speaker = df['Speaker_id'].iloc[0]
    previous_end_time = df['End_time'].iloc[0]
    previous_start_time = df['Start_time'].iloc[0]

    for i in range(1, len(df)):
        current_speaker = df['Speaker_id'].iloc[i]
        current_start_time = df['Start_time'].iloc[i]
        current_end_time = df['End_time'].iloc[i]

        # Calculate speaker-specific talk duration
        if previous_speaker == 'Customer':
            customer_talk_duration += previous_end_time - previous_start_time
        elif previous_speaker == 'Agent':
            agent_talk_duration += previous_end_time - previous_start_time

        # Calculate cross-talk duration
        if current_speaker != previous_speaker:
            if current_start_time < previous_end_time and current_end_time > previous_start_time:
                overlap_start = max(current_start_time, previous_start_time)
                overlap_end = min(current_end_time, previous_end_time)
                overlap_duration = overlap_end - overlap_start

                cross_talk_duration += overlap_duration
                cross_talks += 1
                longest_cross_talk_duration = max(longest_cross_talk_duration, overlap_duration)

        previous_speaker = current_speaker
        previous_end_time = current_end_time
        previous_start_time = current_start_time

    # Calculate talk duration for the last segment
    if previous_speaker == 'Customer':
        customer_talk_duration += previous_end_time - previous_start_time
    elif previous_speaker == 'Agent':
        agent_talk_duration += previous_end_time - previous_start_time

    # Calculate total call duration
    total_call_duration = df['End_time'].max() - df['Start_time'].min()

    return customer_talk_duration, agent_talk_duration, cross_talk_duration, cross_talks, longest_cross_talk_duration, total_call_duration


def analyze_silences(df):
    # Filter the silences from the DataFrame
    df_filtered_silence = df[df['Speaker_id'] == 'Silence']

    # Calculate the duration of each silence
    df_filtered_silence['Duration'] = df_filtered_silence['End_time'] - df_filtered_silence['Start_time']

    # Calculate total number of silences
    total_silences = len(df_filtered_silence)

    # Calculate total duration of all silences
    total_duration = df_filtered_silence['Duration'].sum()

    # Find the longest duration of a silence
    longest_duration = df_filtered_silence['Duration'].max()

    return total_silences, total_duration, longest_duration


