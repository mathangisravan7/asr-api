import pandas as pd
import json


def excel_to_json(excel_file_path, sheet_name, json_file_path):
    try:
        # Read Excel file into pandas DataFrame
        df = pd.read_excel(excel_file_path, sheet_name=sheet_name)

        # Convert DataFrame to JSON
        json_data = df.to_json(orient='records')

        # Write JSON data to a file
        with open(json_file_path, 'w') as json_file:
            json.dump(json.loads(json_data), json_file, indent=4)

        print(f"JSON data has been written to {json_file_path}")

    except Exception as e:
        print("Error:", str(e))


if __name__ == "__main__":
    # Example usage
    excel_file_path = 'uploads/data.xlsx'  # Provide the path to your Excel file
    sheet_name = 'Sheet1'  # Provide the name of the Excel sheet
    json_file_path = 'uploads/data.json'  # Provide the path for the JSON file

    excel_to_json(excel_file_path, sheet_name, json_file_path)